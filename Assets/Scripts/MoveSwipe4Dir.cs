﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveSwipe4Dir : MonoBehaviour
{
    public float speed = 100;

    //public Vector3 startPos;
    //public Vector3 direction;
    //public bool directionChosen;

    float startPosX, startPosY, endPosX, endPosY, diferenciaX, diferenciaY;

    public GameObject [] dodos;


    // Update is called once per frame
    void Update()
    {
        //Para que no se salga de la pantalla
        var pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        transform.position = Camera.main.ViewportToWorldPoint(pos);
    }
    public void OnBeginDrag(PointerEventData data)
    {
        Debug.Log("OnBeginDrag called.");
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("HEYA");
        Debug.Log("Press position + " + eventData.pressPosition);
        Debug.Log("End position + " + eventData.position);
        Vector3 dragVectorDirection = (eventData.position - eventData.pressPosition).normalized;
        Debug.Log("norm + " + dragVectorDirection);
        GetDragDirection(dragVectorDirection);
    }

    private enum DraggedDirection
    {
        Up,
        Down,
        Right,
        Left
    }

    public void OnMouseDown()
    {
        startPosX = Input.mousePosition.x;
        startPosY = Input.mousePosition.y;
        //Debug.Log(Input.mousePosition.x);
        //Debug.Log(Input.mousePosition.y);
    }
    private void OnMouseUp()
    {
        endPosX = Input.mousePosition.x;
        endPosY = Input.mousePosition.y;
        //Debug.Log(Input.mousePosition.x);
        //Debug.Log(Input.mousePosition.y);

        diferenciaX = endPosX + startPosX;
        diferenciaY = endPosY + startPosY;

        Vector3 currentSwipe = new Vector3(endPosX - startPosX, endPosY - startPosY);
        currentSwipe.Normalize();


        //transform.position = Vector3.MoveTowards(transform.position, currentSwipe, speed);

        if (currentSwipe.y > 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
            Debug.Log("up swipe");
            foreach (GameObject dodo in dodos)
            {
                dodo.GetComponent<Animator>().SetInteger("state", 0);
            }
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(0, Screen.height), speed);
        }
        //swipe down
        if (currentSwipe.y < 0 && currentSwipe.x > -0.5f && currentSwipe.x < 0.5f)
        {
            Debug.Log("down swipe");
            foreach (GameObject dodo in dodos)
            {
                dodo.GetComponent<Animator>().SetInteger("state", 0);
            }
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(0, -Screen.height), speed);
        }
        //swipe left
        if (currentSwipe.x < 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            Debug.Log("left swipe");
            foreach (GameObject dodo in dodos)
            {
                dodo.GetComponent<Animator>().SetInteger("state",-1);
            }
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(-Screen.width, 0), speed);
        }
        //swipe right
        if (currentSwipe.x > 0 && currentSwipe.y > -0.5f && currentSwipe.y < 0.5f)
        {
            Debug.Log("right swipe");
            foreach (GameObject dodo in dodos)
            {
                dodo.GetComponent<Animator>().SetInteger("state", 1);
            }
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(Screen.width, 0), speed);
        }




    }


    private DraggedDirection GetDragDirection(Vector3 dragVector)
    {
        float positiveX = Mathf.Abs(dragVector.x);
        float positiveY = Mathf.Abs(dragVector.y);
        DraggedDirection draggedDir;
        if (positiveX > positiveY)
        {
            draggedDir = (dragVector.x > 0) ? DraggedDirection.Right : DraggedDirection.Left;
            Debug.Log("DerechaTodos!");
        }
        else
        {
            draggedDir = (dragVector.y > 0) ? DraggedDirection.Up : DraggedDirection.Down;
            Debug.Log("DerechaTodos!");
        }
        Debug.Log(draggedDir);
        return draggedDir;
    }
}
