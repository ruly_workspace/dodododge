﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSwipe : MonoBehaviour {
    public float speed=100;

    public Vector3 startPos;
    public Vector3 direction;
    public bool directionChosen;

    // Update is called once per frame
    void Update()
    {
        //Para que no se salga de la pantalla
        var pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        transform.position = Camera.main.ViewportToWorldPoint(pos);


        if (Input.GetMouseButtonDown(0))
        {


            startPos = Input.mousePosition;
            directionChosen = false;
        }

        if (Input.GetMouseButtonUp(0))
        {
            direction = Input.mousePosition-startPos;

            directionChosen = true;
        }
        //if (Input.touchCount > 0 )
        //{
        //    Debug.Log("HE");
        //    Touch touch = Input.GetTouch(0);

        //    // Handle finger movements based on touch phase.
        //    switch (touch.phase)
        //    {
        //        // Record initial touch position.
        //        case TouchPhase.Began:
        //            Debug.Log("MATOCAO");
        //            startPos = touch.position;
        //            directionChosen = false;
        //            break;

        //        // Determine direction by comparing the current touch position with the initial one.
        //        case TouchPhase.Moved:

        //            Debug.Log("MAMOVIO");
        //            direction = touch.position - startPos;
        //            break;

        //        // Report that a direction has been chosen when the finger is lifted.
        //        case TouchPhase.Ended:
        //            Debug.Log("MADEJAO");
        //            directionChosen = true;
        //            break;
        //    }


        //}
        //if (directionChosen)
        //{

        transform.position = Vector3.MoveTowards(transform.position, direction, speed);
        //}
    }
}
