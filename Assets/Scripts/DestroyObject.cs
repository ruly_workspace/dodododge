﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

public class DestroyObject : MonoBehaviour {
    public gameManager gm;
	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        print(collision.gameObject.name);
        Destroy(collision.gameObject);
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Dodo")
        {
            if (collision.gameObject.name == "Goal")
            {
                SceneManager.LoadScene("Prototipo");
            }
            else 
            {
                Destroy(collision.gameObject);
                gm.showDodos();
            }
            
        }
        else if (collision.gameObject.name == "ObstacleDestroyer")
        {
            Destroy(this.gameObject);
        }
    }
}
