﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followLeader : MonoBehaviour {

    public Transform lider;
    public float speed=0.03f;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        

        var pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        transform.position = Camera.main.ViewportToWorldPoint(pos);

        transform.position = Vector3.MoveTowards(transform.position, lider.transform.position, speed);
        
        
    }


}
