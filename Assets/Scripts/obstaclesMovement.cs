﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstaclesMovement : MonoBehaviour {
    Vector3 iniPosition,finPosition;
    public float speed =0.03f;
	// Use this for initialization
	void Start () {
        iniPosition = this.gameObject.transform.position;
        finPosition = new Vector3(iniPosition.x, -iniPosition.y);

	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, finPosition, speed);
    }


    public void increaseSpeed()
    {
        speed = speed + 0.03f;
    }

}
