﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour {
    public Text dodosLeft;
    public int dodosTotales = 0;
    public GameObject [] obstacles;
    public obstaclesMovement background,goal;
    // Use this for initialization

    void Start () {
        showDodos();
        InvokeRepeating("increaseSpeedObstacles", 10.0f, 10.0f);
        StartCoroutine(Timer());
    }
	
	// Update is called once per frame
	void Update () {

        //Mucho consumo?
        dodosLeft.text = "Dodos: " + checkDodos();
    }


    public void showDodos()
    {
    }

    public int checkDodos()
    {
        dodosTotales=GameObject.FindGameObjectsWithTag("Dodo").Length;
        if (dodosTotales == 0)
        {
            SceneManager.LoadScene("Prototipo");
        }
        return dodosTotales;
    }

    public void increaseSpeedObstacles()
    {
        background.increaseSpeed();
        goal.speed=background.speed;
        obstacles= GameObject.FindGameObjectsWithTag("Obstacle");
        foreach(GameObject obstacle in obstacles)
        {
            obstacle.GetComponent<obstaclesMovement>().speed = background.speed;
            //obstacle.GetComponent<obstaclesMovement>().increaseSpeed();
        }
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(30);
        this.GetComponent<spawnObstacle>().spawnGoal();
    }


}
