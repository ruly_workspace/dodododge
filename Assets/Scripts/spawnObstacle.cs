﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnObstacle : MonoBehaviour {

    public Transform t1, t2, t3;
    public GameObject obstacle;
    public GameObject goal;
    Transform[] spawnPoints;
    public float spawnTime=3;

    // Use this for initialization
    void Start () {
        spawnPoints = new Transform[3] { t1, t2, t3 };

        InvokeRepeating("SpawnObstacle", spawnTime, spawnTime);
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    void SpawnObstacle()
    {

        int randomNumber = (int)Random.Range(0f, 3f);
        Instantiate(obstacle, spawnPoints[randomNumber].position, spawnPoints[randomNumber].rotation);
    }

    public void spawnGoal()
    {
        Instantiate(goal, spawnPoints[1].position, spawnPoints[1].rotation);
    }
}
